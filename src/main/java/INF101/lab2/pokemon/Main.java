package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        pokemon1 = new Pokemon("Mew", 100, 5);
        pokemon2 = new Pokemon("Oddish", 97, 7);

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive()) { 
                pokemon2.attack(pokemon1);
            }
        }
    }
}
